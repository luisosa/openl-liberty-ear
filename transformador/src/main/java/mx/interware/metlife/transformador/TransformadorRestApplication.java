package mx.interware.metlife.transformador;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 */
@ApplicationPath("/data")
public class TransformadorRestApplication extends Application {
}
