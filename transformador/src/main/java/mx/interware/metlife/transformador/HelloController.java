package mx.interware.metlife.transformador;

import javax.enterprise.inject.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

/**
 *
 */
@Path("/hello")

public class HelloController {

    @GET
    public String sayHello() {
        return "Hello World";
    }
}
